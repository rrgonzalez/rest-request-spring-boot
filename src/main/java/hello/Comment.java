package hello;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class Comment {
	
	public Comment()
	{
		body = "";
		postId = "";
	}
	
	private String id;
    private final String body;
    private final String postId;
}
