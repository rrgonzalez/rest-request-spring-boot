package hello;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class Application {

	private static final Logger log = LoggerFactory.getLogger(Application.class);

	public static void main(String args[]) {
		SpringApplication.run(Application.class);
	}
	
	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}
	
	@Bean
	public CommandLineRunner run(RestTemplate restTemplate) throws Exception {
		return args -> {
			Post post = restTemplate.getForObject(
					"https://json-rest-server.run.goorm.io/posts/1", Post.class);
			log.info(post.toString());
			
			log.info(" ---------------------------------");
			log.info(" Creating new comment...");
			
			String uuid = java.util.UUID.randomUUID().toString();
			Comment newComment = new Comment("New comment - " + uuid, "1");
			
			Comment response = restTemplate.postForObject("https://json-rest-server.run.goorm.io/comments/", newComment, Comment.class);
			log.info(response.toString());
		};
	}
}
